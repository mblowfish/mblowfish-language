# MBlowFish Language

[![pipeline status](https://gitlab.com/mblowfish/mblowfish-language/badges/master/pipeline.svg)](https://gitlab.com/mblowfish/mblowfish-language/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/51dc87e0581d4c9c88ba1dee420dca58)](https://www.codacy.com/app/mblowfish/mblowfish-language?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=mblowfish/mblowfish-language&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/51dc87e0581d4c9c88ba1dee420dca58)](https://www.codacy.com/app/mblowfish/mblowfish-language?utm_source=gitlab.com&utm_medium=referral&utm_content=angular-material-home/angular-material-home-language&utm_campaign=Badge_Coverage)

Adds features to manage languages.

## Install

### Manually

Download content of 'dist' directory of project and place files in your project.

### Bower

Use following command:

	bower install --save mblowfish-language

### Yeoman

## Development

We are using following tools to develop this module. Before starting development you should install below tools.

- nodejs
- npm
- grunt-cli
- bower

### Preparing to develop

Get source of project:

	git clone https://gitlab.com/mblowfish/mblowfish-language.git
	
Now run following commands respectively in root directory of project.

	npm install
	bower install

### Demo 

Run following command to run demo:

	grunt demo

	