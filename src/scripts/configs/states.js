/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

angular.module('mblowfish-language')
/**
 * State of the user management
 */
.config(function($routeProvider) {
	'use strict';
	$routeProvider
	/**
	 * @ngdoc ngRoute
	 * @name /preferences/languages/manager
	 * @description Load language manager
	 * 
	 * Manages languages and allow user to add a new one.
	 */
	.when('/preferences/languages/manager', {
		templateUrl : 'views/amh-setting/languages-manager.html',
		controller : 'MbLanguagesCtrl',
		controllerAs: 'ctrl',
		/*
		 * Check if user is owner
		 * @ngInject
		 */
		protect: function($rootScope){
			var user = $rootScope.app.user;
			return !(user.tenant_owner || user.core_owner || user.Pluf_owner);
		}
	})
	;//
});
